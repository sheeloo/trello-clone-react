import './App.css';
import { useState, useEffect } from 'react';
import { BrowserRouter as Router, Route } from 'react-router-dom'
import Header from './components/Header';
import Boards from './components/Boards';
import Lists from './components/Lists';
import fetchBoards from './api/fetchBoards';
import deleteBoard from './api/deleteBoard';
import createBoard from './api/createBoard';

const { REACT_APP_KEY, REACT_APP_TOKEN, REACT_APP_API } = process.env;


function App() {

  const [boards, setBoards] = useState([]);

  useEffect(() => {

    const getBoards = async () => {
      const boards = await fetchBoards(REACT_APP_API, REACT_APP_KEY, REACT_APP_TOKEN)
      setBoards(boards);
    }
    getBoards();
  }, []);

  const addBoard = async (title) => {

    const board = await createBoard(title, REACT_APP_API, REACT_APP_KEY, REACT_APP_TOKEN)
    setBoards([...boards, board]);
  }

  const delBoard = async (boardId) => {

    await deleteBoard(boardId, REACT_APP_KEY, REACT_APP_TOKEN, REACT_APP_API)
    setBoards(boards.filter(board => board.id !== boardId))
  }


  return (
    <Router>
      <div className="App">
        <Header />

        <Route path='/' exact render={(props) => (
          <>
            {boards.length > 0 ? (
              <Boards
                boards={boards}
                onDelete={delBoard}
                onCreate={addBoard}
              />
            ) : (
              ''
            )}
          </>
        )} />

        <Route path='/b/:id/:name' component={Lists} />
      </div>
    </Router>
  );
}

export default App;
