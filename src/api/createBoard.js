const createBoard = async (board_name, api, key, token) => {

  const promise = await fetch(`${api}boards/?key=${key}&token=${token}&name=${board_name}`, { method: 'POST' })
  const board = await promise.json();

  return board
}

export default createBoard;