const createCard = async (name, idList, api, key, token) => {

    const promise = await fetch(`${api}cards/?key=${key}&token=${token}&name=${name}&idList=${idList}`, { method: 'POST' })
    const card = await promise.json();
  
    return card
    
}

export default createCard
