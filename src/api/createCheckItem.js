const createCheckItem = async (name, checklistId, api, key, token) => {

    const promise = await fetch(`${api}checklists/${checklistId}/checkItems?key=${key}&token=${token}&name=${name}`, { method: 'POST' });

    const checkItem = await promise.json();

    return checkItem;
}

export default createCheckItem
