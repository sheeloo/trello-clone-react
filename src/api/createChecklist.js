const createChecklist = async (name, cardId, api, key, token) => {

    const promise = await fetch(`${api}cards/${cardId}/checklists?key=${key}&token=${token}&name=${name}`, { method: 'POST' });
    const checklist = await promise.json();

    return checklist;

}

export default createChecklist
