const createList = async (name, idBoard, api, key, token) => {

    const promise = await fetch(`${api}lists?key=${key}&token=${token}&name=${name}&idBoard=${idBoard}`, { method: 'POST' })
    const list = await promise.json();

    return list
}

export default createList
