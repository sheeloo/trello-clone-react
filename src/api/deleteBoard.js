const deleteBoard = async ( boardId, key, token, api) => {
  const res = await fetch(`${api}boards/${boardId}?key=${key}&token=${token}`, {method:'DELETE'})
return res;
}

export default deleteBoard;