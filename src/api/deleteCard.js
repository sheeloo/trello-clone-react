const deleteCard = async (cardId, key, token, api) => {

    const res = await fetch(`${api}cards/${cardId}?key=${key}&token=${token}`, { method: 'DELETE' })
    return res;

}

export default deleteCard;