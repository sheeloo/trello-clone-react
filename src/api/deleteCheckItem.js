const deleteCheckItem = async (checkItemId, idChecklist, api, key, token) => {

    const promise = await fetch(`${api}checklists/${idChecklist}/checkItems/${checkItemId}?key=${key}&token=${token}`, { method: 'DELETE' });

    return promise;
}

export default deleteCheckItem
