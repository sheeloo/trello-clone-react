const deleteChecklist = async (cardId, idChecklist, api, key, token) => {

    const res = await fetch(`${api}cards/${cardId}/checklists/${idChecklist}?key=${key}&token=${token}`, { method: 'DELETE' })
    return res;

}

export default deleteChecklist
