const fetchBoards = async (api, key, token) => {
    const res = await fetch(`${api}members/me/boards?key=${key}&token=${token}`)
    const data = await res.json()

    return data
}

export default fetchBoards;