const getBoard = async (boardId, api, key, token) => {

    try {

        let res = await fetch(`${api}boards/${boardId}?key=${key}&token=${token}`)
        const board = await res.json()

        res = await fetch(`${api}boards/${boardId}/lists?key=${key}&token=${token}`)
        const lists = await res.json()
        return {board, lists}

    } catch (error) {
        console.log(error);
    }

}

export default getBoard;