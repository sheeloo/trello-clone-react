const getCards = async (listId, api, key, token) => {

    try {

        let res = await fetch(`${api}lists/${listId}/cards?key=${key}&token=${token}`)
        const cards = await res.json()

        return cards

    } catch (error) {
        console.log(error);
    }

}

export default getCards
