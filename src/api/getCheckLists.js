const getCheckLists = async (cardId, key, token, api) => {
  const res = await fetch(`${api}cards/${cardId}/checklists?key=${key}&token=${token}`)

  const data = res.json();

  return data;
}

export default getCheckLists
