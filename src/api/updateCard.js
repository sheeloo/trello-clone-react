const updateCard = async (cardId, desc, api, key, token) => {

    const promise = await fetch(`${api}cards/${cardId}?key=${key}&token=${token}&desc=${desc}`, { method: 'PUT' });
    const card = await promise.json();

    return card
}

export default updateCard
