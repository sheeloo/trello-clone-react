const updateCheckItem = async (cardId, idChecklist, checkItemId, state, api, key, token) => {
    const promise = await fetch(`${api}cards/${cardId}/checklist/${idChecklist}/checkItem/${checkItemId}?key=${key}&token=${token}&state=${state}`, { method: 'PUT' });
    const checkItem = await promise.json()
    return checkItem
}

export default updateCheckItem
