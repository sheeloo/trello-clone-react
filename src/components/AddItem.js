import './AddItem.css';
import { useState } from 'react'

const AddItem = ({ placeholder, onAddItem, show }) => {

    const [title, setTitle] = useState('');

    const onSubmit = (e) => {
        e.preventDefault()
        onAddItem(title)
        show(false);
        setTitle('');

    }

    return (
        <form onSubmit={onSubmit}>
            <input type='text' className='add-item-title' placeholder={`Enter a title for this ${placeholder}...`}
                value={title} onChange={(e) => setTitle(e.target.value)} required />
            <div className='add-item-button-container'>
                <input type="submit" className='add-button' value='Add' />
                <i className="fa fa-close close-button" aria-hidden="true" onClick={() => show(false)} />
            </div>
        </form>
    )
}

export default AddItem
