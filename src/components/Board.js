import 'font-awesome/css/font-awesome.min.css';
import { Link } from 'react-router-dom'

const Board = ({ board, onDelete }) => {
    const name = board.name.replaceAll(' ', '-');
    const style = {
        backgroundColor: board.prefs.backgroundColor,
        backgroundImage: `url(${board.prefs.backgroundImage})`
    }

    return (

        <div className='boards' style={style}>
            <Link to={`/b/${board.shortLink}/${name}`} ><span>{board.name}</span></Link>
            <i className="fa fa-trash-o fa-lg" aria-hidden="true" onClick={() => onDelete(board.id)} />
        </div>
    )
}

export default Board
