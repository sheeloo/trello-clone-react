import 'font-awesome/css/font-awesome.min.css';
import './BoardInfoBar.css';

function BoardInfoBar({name}) {
    return (
        <section className="board-info-bar">

            <div className="board-controls">
                <button className="board-views-btn btn">
                    <i className="fa fa-trello board-views-btn-icon-left"></i>Board<i className="fa fa-angle-down board-views-btn-icon-right"></i></button>
                <button className="board-title btn">
                    <h2>{name}</h2>
                </button>

                <button className="star-btn btn" aria-label="Star Board">
                    <i className="fa fa-star" aria-hidden="true"></i>
                </button>

                <span className="board-header-btn-divider"></span>

                <button className="board-views-btn btn">
                    <span className="board-header-btn-text">Sheeloo Kushwaha's workspace
                    <span className="org-label">Free</span></span>
                </button>

                <span className="board-header-btn-divider"></span>

                <button className="private-btn btn">
                    <i className="fa fa-briefcase private-btn-icon" aria-hidden="true"></i>Private</button>

                <span className="board-header-btn-divider"></span>

            </div>

            <button className="menu-btn btn"><i className="fa fa-ellipsis-h menu-btn-icon" aria-hidden="true"></i>Show Menu</button>

        </section>
    )
}

export default BoardInfoBar
