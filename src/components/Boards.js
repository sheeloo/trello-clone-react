import Board from './Board';
import CreateBoard from './CreateBoard';
import { useState } from 'react';


const Boards = ({ boards, setBoards, onDelete, onCreate }) => {

    const [showAddBoard, setShowAddBoard] = useState(false);

    return (
        <>
            <main>
                <section className="boards-container">
                    {boards.map((board) => (
                        <Board key={board.id} board={board} onDelete={onDelete} />
                    ))}
                   
                    <div className="boards" aria-label="Create Board" onClick={() => setShowAddBoard(true)}>
                    <button className='btn'>Create new board</button></div>
                    {showAddBoard && <CreateBoard onCreate={onCreate} showModal={setShowAddBoard} />}
                </section>
            </main>

        </>
    )
}

export default Boards
