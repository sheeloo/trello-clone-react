import 'font-awesome/css/font-awesome.min.css';
import CardModal from './CardModal';
import { useState } from 'react';

const Card = ({ card, list, onUpdate, onDelete }) => {

    const [showCard, setShowCard] = useState(false)

    return (
        
        <li>
            <span onClick={(e) => setShowCard(true)}>{card.name}</span>

            <i className="fa fa-trash-o" aria-hidden="true"
                onClick={() => onDelete(card.id)}>
            </i>
            {showCard ? <CardModal card={card} onUpdate={onUpdate} list={list} setShowCard={setShowCard} /> : ''}
        </li>

    )
}

export default Card
