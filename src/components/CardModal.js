import 'font-awesome/css/font-awesome.min.css';
import './CardModal.css';
import SideBar from './SideBar';
import MainCard from './MainCard'
import getCheckLists from '../api/getCheckLists'
import createChecklist from '../api/createChecklist'
import deleteChecklist from '../api/deleteChecklist'
import { useState, useEffect } from 'react';
const { REACT_APP_KEY, REACT_APP_TOKEN, REACT_APP_API } = process.env;

const CardModal = ({ card, list, onUpdate, setShowCard }) => {

    const [checkLists, setCheckLists] = useState([])

    useEffect(() => {

        async function fetchData() {

            const cardCheckLists = await getCheckLists(card.id, REACT_APP_KEY, REACT_APP_TOKEN, REACT_APP_API);
            setCheckLists(cardCheckLists)
        }

        fetchData();

    }, [card.id])

    const addChecklist = async (title) => {

        const checklist = await createChecklist(title, card.id, REACT_APP_API, REACT_APP_KEY, REACT_APP_TOKEN)
        setCheckLists([...checkLists, checklist]);
    }

    const delChecklist = async (idChecklist) => {

        await deleteChecklist(card.id, idChecklist, REACT_APP_API, REACT_APP_KEY, REACT_APP_TOKEN)
        setCheckLists(checkLists.filter(checklist => checklist.id !== idChecklist))

    }

    return (
        <div id="myModal" className="modal">
            <div className="card-modal-content">
                <div className='card-header'>
                    <i className="fa fa-credit-card icon-card" aria-hidden="true"></i>

                    <div className='card-title'>
                        <h2>{card.name}</h2>
                        <span>{`in list ${list.name}`}</span>
                    </div>

                </div>
                <h2 className="card-close-button" onClick={(e) => setShowCard(false)}>
                    <i className="fa fa-close" aria-hidden="true"></i>
                </h2>

                <div className="card-container">
                    <MainCard checklists={checkLists} card={card} onUpdate={onUpdate} onDelete={delChecklist} />
                    <SideBar onAdd={addChecklist} />
                </div>

            </div>
        </div>
    )
}

export default CardModal
