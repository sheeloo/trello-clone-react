import 'font-awesome/css/font-awesome.min.css';
import { useState } from 'react'


const CheckItem = ({ checkItem, onUpdate, onDelete }) => {

    const checked = checkItem.state === "complete" ? true : false

    const [check, setCheck] = useState(checked)

    const checkItemState = (e) => {

        let checkState = e.target.checked ? "complete" : "incomplete";

        const checked = checkState === "complete" ? true : false

        setCheck(checked);

        onUpdate(checkItem.id, checkState);
    }

    return (
        <div className='check-list'>
            <div className="check-item">
                <div><input type="checkbox" className="icon-card" onChange={checkItemState} checked={check} />{checkItem.name}</div>
                <i className="fa fa-trash-o" aria-hidden="true" onClick={() => onDelete(checkItem.id)}></i>
            </div>
        </div>
    )
}

export default CheckItem
