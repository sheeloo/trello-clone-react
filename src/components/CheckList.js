import { useState } from 'react';
import deleteCheckItem from '../api/deleteCheckItem';
import CheckItem from './CheckItem';
import AddItem from './AddItem';
import createCheckItem from '../api/createCheckItem';
import updateCheckItem from '../api/updateCheckItem';
const { REACT_APP_KEY, REACT_APP_TOKEN, REACT_APP_API } = process.env;

const CheckList = ({ checklist, onDelete }) => {

    const [checkItems, setCheckItems] = useState(checklist.checkItems)
    const [showAddItem, setShowAddItem] = useState(false)


    const addCheckItem = async (title) => {

        const checkItem = await createCheckItem(title, checklist.id, REACT_APP_API, REACT_APP_KEY, REACT_APP_TOKEN)
        setCheckItems([...checkItems, checkItem]);
    }

    const onUpdate = async (checkItemId, state) => {

        await updateCheckItem(checklist.idCard, checklist.id, checkItemId, state, REACT_APP_API, REACT_APP_KEY, REACT_APP_TOKEN)
        setCheckItems(checkItems.map((checkItem) =>checkItem.id === checkItemId ? { ...checkItem, state: state } : checkItem ))
    }

    const delCheckItem = async (idCheckItem) => {

        await deleteCheckItem(idCheckItem, checklist.id, REACT_APP_API, REACT_APP_KEY, REACT_APP_TOKEN)
        setCheckItems(checkItems.filter(checkItem => checkItem.id !== idCheckItem))

    }

    return (
        <div className='check-list-container'>
            <div className="check-list">
                <div>
                    <i className="fa fa-check-square icon-card" aria-hidden="true"></i>{checklist.name}
                </div>
                <button className='delete-btn' onClick={() => onDelete(checklist.id)}>DELETE</button>
            </div>
            {checkItems.length > 0 ? checkItems.map(checkItem =>
                <CheckItem key={checkItem.id} checkItem={checkItem} onUpdate={onUpdate} onDelete={delCheckItem} />) : ''}

            {showAddItem ? <AddItem placeholder='checkList' onAddItem={addCheckItem} show={setShowAddItem} />
                : <button className='delete-btn addItem' onClick={(e) => setShowAddItem(true)}>Add an item</button>
            }
        </div>
    )
}

export default CheckList
