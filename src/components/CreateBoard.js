import { useState } from 'react';

const CreateBoard = ({ onCreate, showModal }) => {

    const [title, setTitle] = useState('');

    const onSubmit = (e) => {
        e.preventDefault()

        onCreate(title);
        showModal(false);
        setTitle('');

    }

    return (
        <div id="myModal" className="modal">

            <form className="modal-content" onSubmit={onSubmit}>
                <input type="text" placeholder="Add board title" value={title} onChange={(e) => setTitle(e.target.value)} required />
                <button className="close" onClick={() => showModal(false)}>X</button>
                <input type='submit' value='Create board' className='create-board' />
            </form>

        </div>
    )
}

export default CreateBoard
