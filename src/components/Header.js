import PropTypes from 'prop-types';
import 'font-awesome/css/font-awesome.min.css';
import './Header.css';
import { Link } from 'react-router-dom'


const Header = ({ title }) => {
    return (

        <header className='master-header'>

            <div className='boards-menu'>
               <Link className='home btn'  to='/'><i className="fa fa-th"></i></Link>
               <Link className="home btn" to='/'><i className="fa fa-home"></i></Link>

                <Link className="boards-btn btn" to='/'><i className="fa fa-trello boards-btn-icon"></i>Boards</Link>

                <div className="board-search">
                 <input type="search" className="board-search-input" aria-label="Board Search" placeholder="Jump to..."/>
                 <i className="fa fa-search search-icon" aria-hidden="true"></i>
                 
                </div>
            </div>

            <div className="logo">

            <h1><i className="fa fa-trello logo-icon" aria-hidden="true"></i>Trello</h1>

        </div>


        <div className="user-settings">

            <button className="user-settings-btn btn" aria-label="Create">
              <i className="fa fa-plus" aria-hidden="true"></i>
            </button>

            <button className="user-settings-btn btn" aria-label="Information">
              <i className="fa fa-info-circle" aria-hidden="true"></i>
            </button>

            <button className="user-settings-btn btn" aria-label="Notifications">
              <i className="fa fa-bell" aria-hidden="true"></i>
            </button>

            <button className="user-settings-btn btn" aria-label="User Settings">
              <i className="fa fa-user-circle" aria-hidden="true"></i>
            </button>

        </div>

        </header>
        

    )
}

Header.defaultProps = {
    title: 'Trello'
}

Header.prototype = {
    title: PropTypes.string,
}

export default Header
