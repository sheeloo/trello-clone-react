import 'font-awesome/css/font-awesome.min.css';
import { useState, useEffect } from 'react';
import AddItem from './AddItem';
import Card from './Card';
import createCard from '../api/createCard';
import getCards from '../api/getCards';
import deleteCard from '../api/deleteCard';
import updateCard from '../api/updateCard';
const { REACT_APP_KEY, REACT_APP_TOKEN, REACT_APP_API } = process.env;

const List = ({ list }) => {

    const [showAddItem, setShowAddItem] = useState(false)

    const [cards, setCards] = useState([])

    useEffect(() => {

        async function fetchData() {

            const listCards = await getCards(list.id, REACT_APP_API, REACT_APP_KEY, REACT_APP_TOKEN);
            setCards(listCards);
        }

        fetchData();

    }, [list.id]);


    const addCard = async (title) => {

        const card = await createCard(title, list.id, REACT_APP_API, REACT_APP_KEY, REACT_APP_TOKEN)
        setCards([...cards, card]);
    }

    const delCard = async (cardId) => {

        await deleteCard(cardId, REACT_APP_KEY, REACT_APP_TOKEN, REACT_APP_API)
        setCards(cards.filter(card => card.id !== cardId))
    }

    const onUpdateCard = async (cardId, desc) => {

        await updateCard(cardId, desc, REACT_APP_API, REACT_APP_KEY, REACT_APP_TOKEN)
        setCards(cards.map((card) =>card.id === cardId ? { ...card, desc: desc } : card ))
    }

    return (

        <div className='list'>
            <h3 className='list-title'>{list.name}</h3>
            <ul className='cards'>
                {cards && cards.length > 0 ? cards.map((card) => (
                    <Card key={card.id} card={card} list={list} onUpdate={onUpdateCard} onDelete={delCard} />
                )) : ''}
            </ul>
            {showAddItem ? <AddItem item={cards} placeholder='card' onAddItem={addCard} show={setShowAddItem} /> :
                <button className='add-card-btn' onClick={() => setShowAddItem(true)}>
                    <i className="fa fa-plus add-list-btn-icon" aria-hidden="true"></i>Add a card
                </button>
            }
        </div>
    )
}

export default List
