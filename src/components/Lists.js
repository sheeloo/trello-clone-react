import { useParams } from "react-router-dom";
import getBoard from '../api/getBoard';
import createList from '../api/createList';
import { useState, useEffect } from 'react';
import List from './List';
import AddItem from './AddItem';
import BoardInfoBar from './BoardInfoBar';
const { REACT_APP_KEY, REACT_APP_TOKEN, REACT_APP_API } = process.env;

const Lists = () => {

    let { id, name } = useParams();

    const [idBoard, setIdBoard] = useState('')

    const [lists, setLists] = useState([])

    const [showAddItem, setShowAddItem] = useState(false)

    useEffect(() => {

        async function fetchData() {

            const board = await getBoard(id, REACT_APP_API, REACT_APP_KEY, REACT_APP_TOKEN);

        if (board.board.prefs) {
            setIdBoard(board.board.id);
            const body = document.querySelector('body');
            body.style.backgroundColor = board.board.prefs.backgroundColor;
            body.style.backgroundImage = `url(${board.board.prefs.backgroundImage})`;
            body.style.backgroundAttachment = "fixed";
            body.style.backgroundSize = "cover";
            body.style.backgroundPosition = "center";
            body.style._webkitTextSizeAdjust = "100%";
        }

        setLists(board.lists);

        }
        
        fetchData();

    }, [id]);

    const addList = async (title) => {

        const list = await createList(title, idBoard, REACT_APP_API, REACT_APP_KEY, REACT_APP_TOKEN)
        setLists([...lists, list]);
    }


    return (
        <>
            <BoardInfoBar name={name} />
            <section className="board-lists">

                {lists.length > 0 ? lists.map((list, index) => (
                    <List key={list.id} list={list} />
                )) : ''}

                <div className='add-list-box'>
                    {showAddItem && idBoard !== '' ? <AddItem parentId={idBoard} placeholder='list' onAddItem={addList} show={setShowAddItem} /> :
                        <button className='add-list-btn btn' onClick={() => setShowAddItem(true)}>
                            <i className="fa fa-plus add-list-btn-icon" aria-hidden="true"></i>Add a list
                    </button>
                    }
                </div>

            </section>
        </>
    )
}

export default Lists
