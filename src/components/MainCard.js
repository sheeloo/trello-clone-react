import CheckList from './CheckList';
import { useState } from 'react';
import AddItem from './AddItem';


const MainCard = ({ card, checklists, onUpdate, onDelete }) => {

    const [showAddItem, setShowAddItem] = useState(false);

    const editDescription = (desc) =>{
        onUpdate(card.id, desc);
    }

    return (
        <section className='card-section'>
            <div className="description">
                <i className="fa fa-align-justify icon-card" aria-hidden="true"></i>Description
                <button className="edit-btn" onClick={() => setShowAddItem(true)} >Edit</button>
            </div>

            {showAddItem ? <AddItem placeholder='description' onAddItem={editDescription} show={setShowAddItem} />
                : <p id="descText" className="note">{card.desc}</p>
            }

            <div className="checkList-container">
                {checklists.length > 0 ?
                    checklists.map((checklist) => <CheckList key={checklist.id} checklist={checklist} onDelete={onDelete} />)
                    : ''}
            </div>
        </section>
    )
}

export default MainCard
