import 'font-awesome/css/font-awesome.min.css';
import AddItem from './AddItem'
import { useState } from 'react';

const SideBar = ({ onAdd }) => {

    const [showAddItem, setShowAddItem] = useState(false)
    return (
        <aside>
            <ul>
                <h3>ADD TO CARD</h3>
                <li><i className="fa fa-user fa-sm icon-card" aria-hidden="true"></i>Members</li>
                <li><i className="fa fa-tag fa-sm icon-card" aria-hidden="true"></i>Labels</li>
                {showAddItem ? <AddItem placeholder='checkList' onAddItem={onAdd} show={setShowAddItem} />
                    : <li onClick={(e) => setShowAddItem(true)}><i className="fa fa-check-square fa-sm icon-card" aria-hidden="true"></i>CheckList</li>
                }
                <li><i className="fa fa-paperclip fa-sm icon-card" aria-hidden="true"></i>Attachment</li>

                <h3>ACTIONS</h3>
                <li><i className="fa fa-arrow-right fa-sm icon-card" aria-hidden="true"></i>Move</li>
                <li><i className="fa fa-copy fa-sm icon-card" aria-hidden="true"></i>Copy</li>
                <li><i className="fa fa-eye fa-sm icon-card" aria-hidden="true"></i>Watch</li>
                <li><i className="fa fa-archive fa-sm icon-card" aria-hidden="true"></i>Archive</li>
                <li><i className="fa fa-share-alt fa-xl icon-card" aria-hidden="true"></i>Share</li>

            </ul>
        </aside>
    )
}

export default SideBar
